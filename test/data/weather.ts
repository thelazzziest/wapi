export const LONDON = {
  visibility: 10000,
  name: 'London',
  country: 'GB',
  location: {
    type: 'Point',
    coordinates: [-0.13, 51.51], // lon, lat;
  },
  weather: [
    {
      id: 804,
      main: 'Clouds',
      description: 'overcast clouds',
      icon: '04n',
    },
  ],
  main: {
    temp: 283.08,
    // eslint-disable-next-line @typescript-eslint/camelcase
    feels_like: 278.15,
    // eslint-disable-next-line @typescript-eslint/camelcase
    temp_min: 282.04,
    // eslint-disable-next-line @typescript-eslint/camelcase
    temp_max: 284.26,
    pressure: 1022,
    humidity: 76,
  },
  dt: new Date(
    new Date().getFullYear(),
    new Date().getMonth(),
    new Date().getDate()
  ).getTime(),
  timezone: 0,
};
export const MADRID = {
  location: {
    type: 'Point',
    coordinates: [-3.7, 40.42], // lon, lat;
  },
  weather: [
    {
      id: 803,
      main: 'Clouds',
      description: 'broken clouds',
      icon: '04n',
    },
  ],
  main: {
    temp: 277.09,
    // eslint-disable-next-line @typescript-eslint/camelcase
    feels_like: 274.23,
    // eslint-disable-next-line @typescript-eslint/camelcase
    temp_min: 274.15,
    // eslint-disable-next-line @typescript-eslint/camelcase
    temp_max: 280.37,
    pressure: 1029,
    humidity: 69,
  },
  visibility: 10000,
  dt: new Date(
    new Date().getFullYear(),
    new Date().getMonth(),
    new Date().getDate()
  ).getTime(),
  timezone: 3600,
  name: 'Madrid',
  country: 'ES',
};
