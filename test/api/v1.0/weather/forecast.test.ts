import 'mocha';
import { expect } from 'chai';
import request from 'supertest';

import ExpressServer from '@server/index';
import { USER } from '@test/data/user';
import { LONDON } from '@test/data';

const FORECAST_URL = '/api/v1/weather/forecast';
const LOGIN_URL = '/api/v1/auth/login';

// Valid token
let token: string = null;
describe('Weather', async () => {
  describe('Forecast', async () => {
    beforeEach(async () => {
      const r = await request(ExpressServer)
        .post(LOGIN_URL)
        .send({
          username: USER.username,
          password: USER.password,
        });
      token = r.body.token;
    });

    it(`should get empty response on get to ${FORECAST_URL}`, async () =>
      request(ExpressServer)
        .get(FORECAST_URL)
        .query({
          city: LONDON.name,
          country: LONDON.country,
        })
        .set('Authorization', 'bearer ' + token)
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(404);
          expect(r.body).to.have.property('data').that.is.empty;
        }));

    it(`should get invalid request by date on get to ${FORECAST_URL}`, async () =>
      request(ExpressServer)
        .get(FORECAST_URL)
        .query({
          city: LONDON.name,
          country: LONDON.country,
          date: new Date().toISOString(),
        })
        .set('Authorization', 'bearer ' + token)
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(400);
        }));

    it(`should get forecast for the next day on get to ${FORECAST_URL}`, async () =>
      request(ExpressServer)
        .get(FORECAST_URL)
        .query({
          city: LONDON.name,
          country: LONDON.country,
          date: new Date(
            new Date().getFullYear(),
            new Date().getUTCMonth(),
            new Date().getUTCDay() + 1
          ).toISOString(),
        })
        .set('Authorization', 'bearer ' + token)
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(200);
          expect(r.body).to.have.property('data').that.is.not.empty;
          expect(r.body.data[0].name).to.be.eq(LONDON.name);
        }));
  });
});
