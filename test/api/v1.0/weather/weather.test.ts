import 'mocha';
import { expect } from 'chai';
import request, { Response } from 'supertest';

import App from '@server/index';
import ExpressServer from '@server/common/server';

import { USER, LONDON } from '@test/data';

const WEATHER_URL = '/api/v1/weather/today';
const LOGIN_URL = '/api/v1/auth/login';

// Valid token
let token: string = null;
describe('Weather', async () => {
  beforeEach(async () => {
    const r: Response = await request(App)
      .post(LOGIN_URL)
      .send({
        username: USER.username,
        password: USER.password,
      });
    token = r.body.token;
  });
  describe('Today: empty response', async () => {
    it(`should get empty response on get to ${WEATHER_URL}`, async () =>
      request(ExpressServer)
        .get(WEATHER_URL)
        .query({
          city: 'Paris',
          country: 'FR', // According to iso639-1, iso639-2, iso639-2en or iso639-3
        })
        .set('Authorization', 'bearer ' + token)
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(404);
        }));
  });

  describe('Today: by city name', async () => {
    it(`should get weather condition by city name on get to ${WEATHER_URL}`, async () =>
      request(App)
        .get(WEATHER_URL)
        .query({
          city: LONDON.name,
          country: LONDON.country, // According to iso639-1, iso639-2, iso639-2en or iso639-3
        })
        .set('Authorization', 'bearer ' + token)
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(200);
          expect(r.body)
            .to.have.property('data')
            .that.is.an('array').that.is.not.empty;
          expect(r.body.data[0].name).to.be.eq(LONDON.name);
          expect(r.body.data[0].country).to.be.eq(LONDON.country);
        }));
  });

  describe(`Today: by city coordinates`, async () => {
    it(`should get weather condition by city coordinates on get to ${WEATHER_URL}`, async () =>
      request(App)
        .get(WEATHER_URL)
        .query({
          // London, uk
          lon: LONDON.location.coordinates[0],
          lat: LONDON.location.coordinates[1],
        })
        .set('Authorization', 'bearer ' + token)
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(200);
          expect(r.body)
            .to.have.property('data')
            .that.is.an('array').that.is.not.empty;
          expect(r.body.data[0].name).to.be.eq('London');
          expect(r.body.data[0].country).to.be.eq('GB');
        }));
  });
  // describe('Today: by id', async () => {
  //   let London: WeatherDocument = null;
  //   beforeEach(async () => {
  //     const r: Response = await request(App)
  //       .post(LOGIN_URL)
  //       .send({
  //         username: USER.username,
  //         password: USER.password,
  //       });
  //     token = r.body.token;
  //     London = await WeatherModel.create(LONDON);
  //   });
  //   afterEach(async () => {
  //     if ((await ConditionModel.countDocuments()) > 0) {
  //       await ConditionModel.collection.drop();
  //     }
  //     if ((await WeatherModel.countDocuments()) > 0) {
  //       await WeatherModel.collection.drop();
  //     }
  //   });
  //   it(`should get weather condition by its id on get to ${WEATHER_URL}`, async () => {
  //     request(ExpressServer)
  //       .get(WEATHER_URL)
  //       .query({
  //         id: London.id,
  //       })
  //       .set('Authorization', 'bearer ' + token)
  //       .expect('Content-Type', /json/)
  //       .then(r => {
  //         expect(r.status).to.be.eq(200);
  //         expect(r.body)
  //           .to.have.property('data')
  //           .that.is.an('array').that.is.not.empty;
  //         expect(r.body.data[0].name).to.be.eq(London.name);
  //       });
  //   });
  // });
});
