import 'mocha';
import { expect } from 'chai';
import request from 'supertest';

import ExpressServer from '@server/index';
import { USER } from '@test/data/user';

const LOGIN_URL = '/api/v1/auth/login';

describe('Auth', async () => {
  describe('Login', async () => {
    it(`should successfully login on post to ${LOGIN_URL}`, async () =>
      request(ExpressServer)
        .post(LOGIN_URL)
        .send({
          username: USER.username,
          password: USER.password,
        })
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(200);
          expect(r.body)
            .to.be.an('object')
            .that.has.property('token')
            .that.is.a('string');
        }));
    it(`should be invalid combination of username/password on post to ${LOGIN_URL}`, async () =>
      request(ExpressServer)
        .post(LOGIN_URL)
        .send({
          username: USER.username,
          password: 'notavalidpassword',
        })
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(400);
          expect(r.body)
            .to.be.an('object')
            .that.has.property('errors')
            .that.is.a('array');
        }));
    it(`should be invalid combination of username/password on post to ${LOGIN_URL}`, async () =>
      request(ExpressServer)
        .post(LOGIN_URL)
        .send({
          username: USER.email,
          password: USER.password,
        })
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(400);
          expect(r.body)
            .to.be.an('object')
            .that.has.property('errors')
            .that.is.a('array');
        }));
  });
});
