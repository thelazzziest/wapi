import 'mocha';
import { expect } from 'chai';
import request from 'supertest';

import ExpressServer from '@server/index';
import { USER } from '@test/data/user';

const LOGIN_URL = '/api/v1/auth/login';
const LOGOUT_URL = '/api/v1/auth/logout';

// Valid token
let token: string = null;
const invalidToken: string =
  Math.random()
    .toString(36)
    .substring(4, 8) +
  Math.random()
    .toString(36)
    .substring(4, 8) +
  Math.random()
    .toString(36)
    .substring(4, 8) +
  Math.random()
    .toString(36)
    .substring(8, 16);
describe('Auth', async () => {
  describe('Logout', async () => {
    beforeEach(async () => {
      const r = await request(ExpressServer)
        .post(LOGIN_URL)
        .send({
          username: USER.username,
          password: USER.password,
        });
      token = r.body.token;
    });
    it(`should successfully logout on post to ${LOGIN_URL}`, async () =>
      request(ExpressServer)
        .post(LOGOUT_URL)
        .set('Authorization', 'bearer ' + token)
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(200);
          expect(r.body).to.be.an('string').that.is.empty;
        }));

    it(`token validation should failed on post to ${LOGIN_URL}`, async () =>
      request(ExpressServer)
        .post(LOGOUT_URL)
        .set('Authorization', 'bearer ' + invalidToken)
        .then(r => {
          expect(r.status).to.be.eq(401);
        }));
  });
});
