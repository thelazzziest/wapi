import 'mocha';
import { expect } from 'chai';
import request from 'supertest';

import ExpressServer from '@server/index';

const REGISTER_URL = '/api/v1/auth/register';
const NEWUSER = {
  username: 'new-admin',
  email: 'newadmin@wapi.ru',
  password: '12345678',
  confirm: '12345678',
};
describe('Auth', async () => {
  describe('Register', async () => {
    it(`should successfully post to ${REGISTER_URL}`, () =>
      request(ExpressServer)
        .post(REGISTER_URL)
        .send(NEWUSER)
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(200);
          expect(r.body).to.be.an('object');
          expect(r.body).to.have.property('message');
        }));

    it(`should missing username on post ${REGISTER_URL}`, async () =>
      request(ExpressServer)
        .post(REGISTER_URL)
        .send({
          email: NEWUSER.email,
          password: NEWUSER.password,
          confirm: NEWUSER.confirm,
        })
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(400);
          expect(r.body).to.be.an('object');
          expect(r.body)
            .to.have.property('errors')
            .that.is.an('array');
        }));

    it(`should missing email on post ${REGISTER_URL}`, () =>
      request(ExpressServer)
        .post(REGISTER_URL)
        .send({
          username: NEWUSER.username,
          password: NEWUSER.password,
          confirm: NEWUSER.confirm,
        })
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(400);
          expect(r.body).to.be.an('object');
          expect(r.body)
            .to.have.property('errors')
            .that.is.an('array');
        }));

    it(`email should be invalid on post ${REGISTER_URL}`, async () =>
      request(ExpressServer)
        .post(REGISTER_URL)
        .send({
          email: 'newadmin-wapi.ru',
          password: NEWUSER.password,
          confirm: NEWUSER.confirm,
        })
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(400);
          expect(r.body).to.be.an('object');
          expect(r.body)
            .to.have.property('errors')
            .that.is.an('array');
        }));

    it(`password should be missing on post ${REGISTER_URL}`, async () =>
      request(ExpressServer)
        .post(REGISTER_URL)
        .send({
          email: 'newadmin-wapi.ru',
        })
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(400);
          expect(r.body).to.be.an('object');
          expect(r.body)
            .to.have.property('errors')
            .that.is.an('array');
        }));
    it(`password confirm should be missing on post ${REGISTER_URL}`, async () =>
      request(ExpressServer)
        .post(REGISTER_URL)
        .send({
          email: 'newadmin-wapi.ru',
          password: NEWUSER.password,
        })
        .expect('Content-Type', /json/)
        .then(r => {
          expect(r.status).to.be.eq(400);
          expect(r.body).to.be.an('object');
          expect(r.body)
            .to.have.property('errors')
            .that.is.an('array');
        }));
  });
});
