import { UserDocument, UserModel } from '@server/user';
import { USER } from '@test/data/user';

export async function createAdmin(): Promise<void> {
  const admin: UserDocument = new UserModel({
    username: USER.username,
    email: USER.email,
  });
  await UserModel.register(admin, USER.password);
}
export async function dropAdmin(): Promise<void> {
  if ((await UserModel.countDocuments()) > 0) {
    await UserModel.collection.drop();
  }
}
