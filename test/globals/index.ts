import { createAdmin, dropAdmin } from './user';
import { createLocation, dropLocation } from './weather';

before(async () => {
  await createAdmin();
  await createLocation();
});
after(async () => {
  await dropAdmin();
  await dropLocation();
});
