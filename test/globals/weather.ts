import { WeatherModel } from '@server/weather';
import { LONDON, MADRID } from '@test/data';

export async function createLocation(): Promise<void> {
  const london = new WeatherModel(LONDON);
  await london.save();
  const madrid = new WeatherModel(MADRID);
  await madrid.save();
}
export async function dropLocation(): Promise<void> {
  if ((await WeatherModel.countDocuments()) > 0) {
    await WeatherModel.collection.drop();
  }
}
