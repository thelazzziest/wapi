import request from 'request-promise';
import OWMPathBuilder from './builder';

export default class OWMAPI {
  constructor(key, opts, ...props) {
    this.pathBuilder = new OWMPathBuilder(key, ...props);
    this.defaultOpts = opts;
    this.headers = {};
  }

  /**
   * Add custom headers
   * @param headers
   * @returns {OWMAPI}
   */
  setHeaders(headers) {
    this.headers = headers;
    return this;
  }

  request(uri, queryOptions) {
    return request.defaults(this.defaultOpts).get(uri, queryOptions);
  }

  /**
   * Get weather data
   * @param queryOpts
   * @returns RequestPromise
   */
  getWeather(queryOpts) {
    const uri = this.pathBuilder
      .weather(queryOpts)
      .build()
      .toString();
    const params = {
      headers: this.headers,
    };
    return this.request(uri, params);
  }

  /**
   * Get forecast data
   * @param queryOpts
   * @returns RequestPromise
   */
  getForecast(queryOpts) {
    const path = this.pathBuilder
      .forecast(queryOpts)
      .build()
      .toString();
    const params = {
      headers: this.headers,
    };
    return this.request(path, params);
  }
}
