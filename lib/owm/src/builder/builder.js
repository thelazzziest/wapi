import { URL } from 'url';

export default class AbstractBuilder {

  /**
   * Build the base or API uri
   * @returns {string}
   */
  base() {
    return `${this.host}${this.rootPath}`;
  }

  /**
   * Attach authentication token
   * @param url
   * @returns {string}
   */
  authenticate(url) {
    url += url.includes('?') ? '&' : '?';
    return `${url}APPID=${this.key}`;
  }


  /**
   * Attach api version
   * @param url
   * @returns {string}
   */
  versioning(url) {
    return `${url}/${this.version}`;
  }

  /**
   * Attach the specified resource path and query parameters if they exist
   * @param url
   * @returns {string}
   */
  resourcing(url) {
    return `${url}/${this.resource.build()}`;
  }

  /**
   * Build the whole api uri string
   */
  build() {
    const url = this.authenticate(
      this.resourcing(this.versioning(this.base()))
    );
    return new URL(url);
  }
}
