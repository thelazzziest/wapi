import { stringify } from 'querystring';

export default class AbstractResource {
  constructor() {
    this.path = null;
    this.opts = {};
  }

  build() {
    return this.path + '?' + stringify(this.opts);
  }
}
