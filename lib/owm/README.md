# OWM API - Openweathermap API

# Quick start

## Basic Usage
```ecmascript 6
const owm = new OWMAPI('owm_api_key');
owm.getWeather({
    q: 'London,uk'
}).then(res => {
    console.log('Openweather api response:', res.body);
})
```