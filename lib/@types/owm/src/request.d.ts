import RequestPromise = require('request-promise');
import OWMPathBuilder = require('./builder');

interface OWMInterface {
  pathBuilder: OWMPathBuilder;
  headers: Record<string, string>;
  request: RequestPromise.RequestPromise;

  setHeaders(headers: Array<Record<string, string>>): OWMAPI;
  getWeather(opts: Record<string, string>): RequestPromise.RequestPromise;
  getForecast(opts: Record<string, string>): RequestPromise.RequestPromise;
}

declare class OWMAPI implements OWMInterface {
  headers: Record<string, string>;
  pathBuilder: OWMPathBuilder;
  request: RequestPromise.RequestPromise;

  constructor(key: string, ...opts: any);

  setHeaders(headers: Array<Record<string, string>>): OWMAPI;

  getForecast(opts: Record<string, string>): RequestPromise.RequestPromise;

  getWeather(opts: Record<string, string>): RequestPromise.RequestPromise;
}
export = OWMAPI;
