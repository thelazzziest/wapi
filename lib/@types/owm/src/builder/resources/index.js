import ForecastResource from './forecast';
import WeatherResource from './weather';
export { ForecastResource, WeatherResource };
