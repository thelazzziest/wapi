import AbstractResource from './resource';

export default class ForecastResource extends AbstractResource {
  /**
   * Forecast endpoint
   * @param opts
   */
  forecast(opts) {
    this.path = 'forecast';
    this.opts = opts;
  }
}
