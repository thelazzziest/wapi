import { stringify } from 'querystring';

export default class WeatherResource {
  constructor() {
    this.path = null;
    this.opts = {};
  }

  /**
   * Weather endpoint
   * @param opts object Query parameters.See: https://openweathermap.org/current#one
   */
  weather(opts) {
    this.path = 'weather';
    this.opts = opts;
  }

  build() {
    return this.path + '?' + stringify(this.opts);
  }
}
