declare class AbstractBuilder {
  /**
   * Build the base or API uri
   * @returns {string}
   */
  base(): string;

  /**
   * Attach authentication token
   * @param url
   * @returns {string}
   */
  authenticate(url: string): string;

  /**
   * Attach api version
   * @param url
   * @returns {string}
   */
  versioning(url: string): string;

  /**
   * Attach the specified resource path and query parameters if they exist
   * @param url
   * @returns {string}
   */
  resourcing(url: string): string

  /**
   * Build the whole api uri string
   */
  build(): string;
}
export = AbstractBuilder;