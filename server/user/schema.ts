import mongoose, { Schema, HookNextFunction } from 'mongoose';
import passportLocalMongoose from 'passport-local-mongoose';

import { UserDocument, PassportUserModel, UserLocalSchema } from './interface';

const UserSchema: Schema = new Schema({
  username: { type: String, required: true },
  firstName: { type: String, required: false },
  lastName: { type: String, required: false },
  email: { type: String, required: true, unique: true },
  password: { type: String },
  createdAt: Date,
}) as UserLocalSchema;

UserSchema.plugin(passportLocalMongoose);
UserSchema.pre('save', function(
  this: UserDocument,
  next: HookNextFunction,
  docs: any[]
): Promise<UserDocument> {
  if (!!this.createdAt) {
    this.createdAt = new Date();
  }
  return next();
});
UserSchema.methods.fullName = function(): string {
  return `${this.firstName} ${this.lastName}`;
};

export const UserModel: PassportUserModel = mongoose.model<
  UserDocument,
  PassportUserModel
>('User', UserSchema);
