import {PassportLocalDocument, PassportLocalModel, PassportLocalSchema} from 'mongoose';

export interface UserDocument extends PassportLocalDocument {
  firstName?: string;
  lastName?: string;
  username: string;
  email: string;
  password: string;
  createdAt: Date;
}

export interface UserLocalSchema extends PassportLocalSchema {
  firstName?: string;
  lastName?: string;
  username: string;
  email: string;
  password: string;
  createdAt: Date;

  fullName(): string;
}

export interface PassportUserModel extends PassportLocalModel<UserDocument> {
  fullName(): string;
}
