import mongoose, { NativeError, Schema } from 'mongoose';

import {
  ConditionDocument,
  ConditionModelInterface,
  WeatherDocument,
  WeatherModelInterface,
} from './interface';
import l from '@server/common/logger';

const ConditionSchema: Schema = new Schema({
  id: { type: Number, required: true },
  main: { type: String, required: true },
  description: { type: String, required: true },
  icon: { type: String, required: false },
});

const CloudSchema: Schema = new Schema({
  all: { type: Number, required: true },
});

const TemperatureSchema: Schema = new Schema({
  temp: { type: Number, required: true },
  // eslint-disable-next-line @typescript-eslint/camelcase
  temp_min: { type: Number, required: true },
  // eslint-disable-next-line @typescript-eslint/camelcase
  temp_max: { type: Number, required: true },
  // eslint-disable-next-line @typescript-eslint/camelcase
  temp_kf: { type: Number, required: false },
  // eslint-disable-next-line @typescript-eslint/camelcase
  feels_like: { type: Number, required: true },
  pressure: { type: Number, required: true },
  // eslint-disable-next-line @typescript-eslint/camelcase
  sea_level: { type: Number, required: false },
  // eslint-disable-next-line @typescript-eslint/camelcase
  grnd_level: { type: Number, required: false },
  humidity: { type: Number, required: true },
});

const WeatherSchema: Schema = new Schema({
  visibility: { type: Number, required: false },
  // Location related fields fields
  name: { type: String, required: true },
  country: { type: String, required: true },
  location: {
    type: {
      type: String,
      required: true, // 'location.type' must be 'Point'
      enum: ['Point'],
    },
    coordinates: {
      type: [Number],
      required: true,
    },
  },
  // Condition related data
  weather: {
    type: [ConditionSchema],
    required: true,
  },

  // Weather condition temperature data
  main: { type: TemperatureSchema, required: true },
  clouds: { type: CloudSchema, required: false },
  dt: { type: Number, required: false },
  // eslint-disable-next-line @typescript-eslint/camelcase
  dt_txt: { type: Date, required: false },
  timezone: { type: Number, required: true },
});
WeatherSchema.index({ name: 1, country: 1, dt: 1 }, { unique: true });
WeatherSchema.index({
  location: '2dsphere',
});
WeatherSchema.on('index', err => {
  if (err) {
    l.error('User index error: %s', err);
  } else {
    l.info('User indexing complete');
  }
});

WeatherSchema.post(
  'save',
  (docs: WeatherDocument, next: (err?: NativeError) => void) => {
    if (!docs.dt_txt) {
      // eslint-disable-next-line @typescript-eslint/camelcase
      docs.dt_txt = new Date(docs.dt);
    }
    return next();
  }
);

const ConditionModel: ConditionModelInterface = mongoose.model<
  ConditionDocument,
  ConditionModelInterface
>('Condition', ConditionSchema);

const WeatherModel: WeatherModelInterface = mongoose.model<
  WeatherDocument,
  WeatherModelInterface
>('Weather', WeatherSchema);

WeatherModel.ensureIndexes((err: any) => {
  if (err) {
    l.error('Weather model:', err);
  }
});
export { WeatherModel, ConditionModel };
