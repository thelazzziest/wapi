import Agenda from 'agenda';
import { OWMAPI } from '@lib/owm';

import ServerConfig from '@server/common/config';
import agenda from '@server/common/agenda';
import l from '@server/common/logger';

import { WeatherModel } from './schema';
import { WeatherDocument } from './interface';
import ConditionUtils from '@server/api/v1.0/weather/utils';

interface Worker {
  api: OWMAPI;
  addWorkers(queue: Agenda): void;
}

class AbstractWorker implements Worker {
  public readonly api: OWMAPI;

  constructor(config: ServerConfig, agenda: Agenda) {
    this.api = new OWMAPI(config.OWM_APP_ID);
    this.addWorkers(agenda);
  }
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  addWorkers(queue: Agenda): void {}
}

export class ForecastWorker extends AbstractWorker {
  public addWorkers(queue: Agenda): void {
    agenda.define('owm:forecast', async () => {
      const cities: Array<WeatherDocument> = await WeatherModel.find().select(
        'name country'
      );
      if (cities.length == 0) {
        l.info('owm:forecast', 'No cities found. Abort updating.');
        return;
      }
      for (const row of cities) {
        const res = await this.api.getWeather({
          q: `${row.name},${row.country}`,
        });
        if (res.cod != 200) {
          l.error('Forecast:', res);
          return;
        }
        for (const entry of res.list) {
          const conditions = await ConditionUtils.getWeatherConditions(
            entry.weather
          );
          const forecast: WeatherDocument = new WeatherModel({
            name: res.city.name,
            country: res.city.country,
            location: {
              type: 'Point',
              coordinates: [entry.coord.lon, entry.coord.lat],
            },
            weather: conditions,
            main: entry.main,
            dt: entry.dt,
            // eslint-disable-next-line @typescript-eslint/camelcase
            dt_txt: entry.dt_txt,
            timezone: res.city.timezone,
          });
          await forecast.save();
        }
      }
    });
  }
}

export class WeatherWorker extends AbstractWorker {
  public addWorkers(queue: Agenda): void {
    agenda.define('owm:weather', async () => {
      const cities: Array<WeatherDocument> = await WeatherModel.find().select(
        'name country'
      );
      if (cities.length == 0) {
        l.info('owm:weather', 'No cities found. Abort updating.');
        return;
      }
      for (const row of cities) {
        try {
          const res = await this.api.getWeather({
            q: `${row.name},${row.country}`,
          });
          if (res.cod != 200) {
            l.error('Weather: ', res);
            return;
          }

          const conditions = await ConditionUtils.getWeatherConditions(
            res.weather
          );
          const weather: WeatherDocument = new WeatherModel({
            visibility: res.visibility,
            name: res.name,
            country: res.sys.country,
            location: {
              type: 'Point',
              coordinates: [res.coord.lon, res.coord.lat],
            },
            weather: conditions,
            main: res.main,
            dt: res.dt,
            // eslint-disable-next-line @typescript-eslint/camelcase
            dt_txt: res.dt_txt,
            timezone: res.timezone,
          });
          await weather.save();
          return Promise.resolve(weather);
        } catch (e) {
          l.warn('owm:weather', e);
        }
      }
    });
  }
}
