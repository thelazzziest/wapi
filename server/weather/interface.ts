import { Document, Model } from 'mongoose';

export interface ConditionDocument extends Document {
  id: number;
  main: string;
  description: string;
  icon: string;
}

export interface LocationDocument extends Document {
  type: string;
  coordinates: Array<number>;
}

export interface ConditionModelInterface extends Model<ConditionDocument> {}

export interface TemperatureDocument extends Document {
  temp: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  humidity: number;
  sea_level?: number;
  grnd_level?: number;
  temp_kf?: number;
}

export interface Wind extends Document {
  speed: number;
  deg: number;
}

export interface Sys extends Document {
  type?: number;
  pod?: string;
}

export interface WeatherDocument extends Document {
  visibility: number;
  name: string;
  country: string;
  location: LocationDocument;
  main: TemperatureDocument;
  weather: Array<ConditionDocument>;
  clouds: Record<string, number>;
  wind: Record<string, string>;
  snow: Record<string, number>;
  dt: number;
  dt_txt: Date;
}

export interface WeatherModelInterface extends Model<WeatherDocument> {}
