import path from 'path';
import os from 'os';

import http from 'http';
import express, { Express } from 'express';

import Agendash from 'agendash';

import Agenda from 'agenda';
import cors from 'cors';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import passport from 'passport';
import passportJWT, { VerifiedCallback } from 'passport-jwt';
import passportLocal from 'passport-local';

import './env';
import l from './logger';
import ServerConfig from './config';
import initOpenapi from './openapi';
import initDb from './mongo';
import agenda from './agenda';

import ApiRouter from '@server/api';

import { UserModel } from '@server/user';
import { WeatherWorker, ForecastWorker } from '@server/weather/';

interface Server {
  readonly root: string;
  readonly app: Express;
  readonly config: ServerConfig;
  readonly queue: Agenda;
}

export default class ExpressServer implements Server {
  public readonly root: string = path.normalize(__dirname + '/../..');

  public readonly app: Express;
  public readonly config: ServerConfig;
  public readonly queue: Agenda;

  constructor() {
    this.app = express();
    this.config = ServerConfig.getInstance();
    this.queue = agenda;
  }

  private express(): ExpressServer {
    this.app.set('appPath', this.root + 'client');
    this.app.use(bodyParser.json({ limit: this.config.REQUEST_LIMIT }));
    this.app.use(
      bodyParser.urlencoded({
        extended: true,
        limit: this.config.REQUEST_LIMIT,
      })
    );
    this.app.use(cors());
    this.app.use(helmet());

    this.app.use(bodyParser.text({ limit: this.config.REQUEST_LIMIT }));
    this.app.use(cookieParser(this.config.SESSION_SECRET));
    this.app.use(express.static(`${this.root}/public`));

    return this;
  }

  private openapi(): ExpressServer {
    const install = async () => {
      await initOpenapi(this);
    };
    install().catch(error => l.error('Openapi:', error));
    return this;
  }

  private passport(): ExpressServer {
    const LocalStrategy = passportLocal.Strategy,
      JWTStrategy = passportJWT.Strategy,
      ExtractJWT = passportJWT.ExtractJwt;

    this.app.use(passport.initialize());

    // Initialize passport strategies
    // Passport strategy for authenticating with a username and password.
    passport.use(
      new LocalStrategy(
        {
          usernameField: this.config.USERNAME_FIELD,
          passwordField: this.config.PASSWORD_FIELD,
        },
        UserModel.authenticate()
      )
    );
    // Passport strategy for authentication with a JWT token
    passport.use(
      new JWTStrategy(
        {
          jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
          secretOrKey: this.config.JWT_SECRET,
        },
        async (payload: any, done: VerifiedCallback): Promise<void> => {
          try {
            const user = await UserModel.findById(payload.id);
            if (user) {
              return done(null, user);
            } else {
              return done(null, false);
            }
          } catch (err) {
            return done(err, false);
          }
        }
      )
    );

    // User data processing
    passport.serializeUser(UserModel.serializeUser());
    passport.deserializeUser(UserModel.deserializeUser());

    return this;
  }

  private mongo(): ExpressServer {
    (async () => {
      await initDb(err => {
        if (err) {
          l.error('MongoDB:', err);
          process.exit(1);
        }
      });
    })();
    return this;
  }

  /**
   * Initialize weather app.
   */
  private weather(): ExpressServer {
    // Init jobs
    new WeatherWorker(this.config, this.queue);
    new ForecastWorker(this.config, this.queue);
    return this;
  }

  private agenda(): ExpressServer {
    this.queue.every('0 0 * * *', ['owm:weather']);
    this.queue.every('0 */6 * * *', ['owm:forecast']);

    (async (ctx: ExpressServer) => {
      await ctx.queue.start();
      l.info('Agenda:', 'Jobs are up and running.');
    })(this);
    this.app.use('/process', Agendash(this.queue));
    return this;
  }

  private routes(): ExpressServer {
    new ApiRouter(this);
    return this;
  }

  public create(): ExpressServer {
    this.mongo()
      .express()
      .weather()
      .agenda()
      .passport()
      .openapi()
      .routes();

    return this;
  }

  public listen(): Express {
    const welcome = (p: number, env: string) => (): void => {
      const hostname = os.hostname();
      const path = `http://${hostname}:${p}/`;
      l.info(
        `Host ${path} is up and running in ${env} @: ${os.hostname()} on port: ${p}}`
      );
    };

    try {
      http
        .createServer(this.app)
        .listen(
          this.config.SERVER_PORT,
          welcome(this.config.SERVER_PORT, this.config.NODE_ENV)
        );
    } catch (e) {
      l.error(e);
      process.exit(1);
    }
    return this.app;
  }
}
