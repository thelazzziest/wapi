export default class ServerConfig {
  private static instance: ServerConfig;

  public readonly NODE_ENV: string;
  public readonly SERVER_PORT: number;

  // Body parser serttings
  public readonly REQUEST_LIMIT: string;
  public readonly SESSION_SECRET: string;

  // Redis settings
  public readonly REDIS_HOST: string;
  public readonly REDIS_PORT: number;

  // Openapi settings
  public readonly OPENAPI_SPEC: string;
  public readonly OPENAPI_ENABLE_RESPONSE_VALIDATION: boolean;

  // MongoDB settings
  public readonly MONGO_DEBUG: boolean;
  public readonly MONGO_HOST: string;
  public readonly MONGO_PORT: number;
  public readonly MONGO_DB: string;
  public readonly MONGO_AUTO_RECONNECT: boolean;

  // Pino
  public readonly APP_ID: string;
  public readonly LOG_LEVEL: string;

  // Authentication
  public readonly USERNAME_FIELD: string;
  public readonly PASSWORD_FIELD: string;
  public readonly JWT_SECRET: string;

  // Bull
  public readonly BULL_QUEUE_PREFIX: string;

  // Openweathermap
  public readonly OWM_APP_ID: string;

  private constructor(process: NodeJS.Process) {
    this.NODE_ENV = process.env.NODE_ENV || 'development';

    this.SESSION_SECRET = process.env.SESSION_SECRET;
    this.REQUEST_LIMIT = process.env.REQUEST_LIMIT || '100kb';
    this.REDIS_HOST = process.env.REDIS_HOST;
    this.REDIS_PORT = parseInt(process.env.REDIS_PORT);

    this.SERVER_PORT = parseInt(process.env.PORT);

    this.OPENAPI_SPEC = process.env.OPENAPI_SPEC || '/spec';
    this.OPENAPI_ENABLE_RESPONSE_VALIDATION = !!(
      process.env.OPENAPI_ENABLE_RESPONSE_VALIDATION &&
      process.env.OPENAPI_ENABLE_RESPONSE_VALIDATION.toLowerCase() == 'true'
    );

    this.MONGO_DEBUG = !!process.env.MONGO_DEBUG || false;
    this.MONGO_HOST = process.env.MONGO_HOST;
    this.MONGO_PORT = parseInt(process.env.MONGO_PORT);
    this.MONGO_DB = process.env.MONGO_DB || '';
    this.MONGO_AUTO_RECONNECT = !!(
      process.env.MONGO_AUTO_RECONNECT &&
      process.env.MONGO_AUTO_RECONNECT.toLowerCase() == 'true'
    );

    this.APP_ID = process.env.APP_ID;
    this.LOG_LEVEL = process.env.LOG_LEVEL || 'error';

    this.USERNAME_FIELD = process.env.USERNAME_FIELD || 'username';
    this.PASSWORD_FIELD = process.env.PASSWORD_FIELD || 'password';
    this.JWT_SECRET = process.env.JWT_SECRET;

    this.BULL_QUEUE_PREFIX = process.env.KUE_QUEUE_PREFIX || 'wapi';

    this.OWM_APP_ID = process.env.OWN_APP_ID;
  }

  public static getInstance(): ServerConfig {
    if (!ServerConfig.instance) {
      ServerConfig.instance = new ServerConfig(process);
    }
    return ServerConfig.instance;
  }
}
