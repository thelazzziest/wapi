import Agenda from 'agenda';

import ServerConfig from '@server/common/config';

const config = ServerConfig.getInstance();

const MONGO_URI = `mongodb://${config.MONGO_HOST}:${config.MONGO_PORT}/${config.MONGO_DB}`;
export default new Agenda({
  db: { address: MONGO_URI },
});
