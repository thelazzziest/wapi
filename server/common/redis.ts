import IORedis from 'ioredis';
import ServerConfig from './config';
import l from './logger';

const config = ServerConfig.getInstance();
const redis = new IORedis(config.REDIS_PORT, config.REDIS_HOST);
redis.on('error', async error => {
  l.error('Redis:', 'Error occurred - ', error);
});
redis.on('connect', async () => {
  l.info(
    'Redis:',
    `Connection has been established on port ${config.REDIS_PORT}`
  );
});

export default redis;
