import * as path from 'path';
import express, { NextFunction, Request, Response } from 'express';
import { OpenApiValidator } from 'express-openapi-validator';
import ExpressServer from './server';

function errorHandler(
  err: any,
  req: Request,
  res: Response,
  next: NextFunction
): void {
  const errors = err.errors || [{ message: err.message }];
  res.status(err.status || 500).json({ errors });
}

export default async function(server: ExpressServer): Promise<void> {
  const apiSpec = path.join(path.dirname(__dirname), 'api.yml');
  const validateResponses = server.config.OPENAPI_ENABLE_RESPONSE_VALIDATION;
  const validator = new OpenApiValidator({ apiSpec, validateResponses });

  await validator.install(server.app);
  server.app.use(server.config.OPENAPI_SPEC, express.static(apiSpec));
  server.app.use(errorHandler);
}
