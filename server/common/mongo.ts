import mongoose from 'mongoose';
import mongodb from 'mongodb';

import l from './logger';
import ServerConfig from './config';

const config = ServerConfig.getInstance();
const MONGO_URI = `mongodb://${config.MONGO_HOST}:${config.MONGO_PORT}/${config.MONGO_DB}`;

mongoose.set('debug', config.MONGO_DEBUG);
mongoose.connection.once('open', async () => {
  l.info(
    'MongoDB:',
    `Connection has been established on port ${config.MONGO_PORT}`
  );
});
mongoose.connection.on('error', async err => {
  l.error('MongoDB:', 'Error occurred - ', err);
});

export default async (onError: (err: mongodb.MongoError) => void) => {
  await mongoose.connect(
    MONGO_URI,
    {
      useNewUrlParser: true,
      autoReconnect: config.MONGO_AUTO_RECONNECT,
      useUnifiedTopology: true,
      useCreateIndex: true,
    },
    onError
  );
};
