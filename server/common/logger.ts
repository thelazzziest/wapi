import pino from 'pino';
import ServerConfig from './config';

const config = ServerConfig.getInstance();

const l = pino({
  name: config.APP_ID,
  level: config.LOG_LEVEL,
  prettyPrint: {
    colorize: true,
    levelFirst: false,
  },
});

export default l;
