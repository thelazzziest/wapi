/// <reference types="node" />
declare module 'agendash' {
  import Agenda from 'agenda';
  import { Express } from 'express';

  interface AgendashOptions {
    middelware?: string;
  }
  function init(agenda: Agenda, options?: AgendashOptions): Express;
  export = init;
}
