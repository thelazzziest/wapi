require('module-alias/register');
import Server from './common/server';

export default new Server().create().listen();
