import ExpressServer from '@server/common/server';

import { V1Router } from './v1.0';

export default class ApiRouter {
  constructor(ctx: ExpressServer) {
    ctx.app.use('/api/v1', new V1Router(ctx).router);
  }
}
