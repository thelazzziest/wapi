import { Request, Response, NextFunction } from 'express';
import { check, validationResult } from 'express-validator';

export default class AuthValidator {
  public static readonly registerValidator = [
    check('username')
      .isString()
      .trim()
      .not()
      .notEmpty()
      .exists(),
    check('email')
      .isString()
      .trim()
      .notEmpty()
      .isEmail()
      .exists(),
    check('confirm')
      .isString()
      .trim()
      .notEmpty()
      .exists(),
    check('password')
      .isString()
      .trim()
      .notEmpty()
      .isLength({ min: 8 })
      .custom((password: string, meta: object) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        if (password != meta.req.body.confirm) {
          throw new Error("Passwords don't match");
        } else {
          return password;
        }
      })
      .exists(),
    check('firstName')
      .isString()
      .optional(),
    check('lastName')
      .isString()
      .optional(),
  ];

  public static readonly loginValidator = [
    check('username')
      .isString()
      .trim()
      .notEmpty(),
    check('password')
      .isString()
      .trim()
      .notEmpty()
      .isLength({ min: 8 }),
  ];

  public static async validate(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void | Response> {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
      return next();
    }
    const extractedErrors: any[] = [];
    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }));

    return res.status(400).json({
      errors: extractedErrors,
    });
  }
}
