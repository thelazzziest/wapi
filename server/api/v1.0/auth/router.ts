import { Router } from 'express';

import AuthValidator from './validator';
import AuthController from './controller';
import passport from 'passport';

export default class AuthRouter {
  public readonly router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  private routes() {
    this.router.post(
      '/register',
      AuthValidator.registerValidator,
      AuthValidator.validate,
      AuthController.register
    );
    this.router.post(
      '/login',
      AuthValidator.loginValidator,
      AuthValidator.validate,
      AuthController.login
    );
    this.router.post(
      '/logout',
      passport.authenticate('jwt', { session: false }),
      AuthController.logout
    );
  }
}
