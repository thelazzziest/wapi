import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import passport from 'passport';

import l from '@server/common/logger';
import ServerConfig from '@server/common/config';

import { UserDocument, UserModel } from '@server/user';

import AuthErrors from './errors';

export default class AuthController {
  private static readonly config: ServerConfig = ServerConfig.getInstance();

  public static async register(req: Request, res: Response): Promise<Response> {
    try {
      const user: UserDocument = new UserModel({
        username: req.body.username,
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
      });
      const account: UserDocument = await UserModel.register(
        user,
        req.body.password
      );
      l.info(`User ${account.email} has been created.`);
      return res.status(200).json({
        message: 'Successfully created new account',
      });
    } catch (err) {
      l.error('Register:', err.message);
      return res.status(500).json({
        errors: [
          {
            error: 'InternalServerError',
            message: 'Internal server error.',
          },
        ],
      });
    }
  }

  public static async login(req: Request, res: Response): Promise<Response> {
    try {
      const afterVerify = async (
        err: any,
        user: any,
        info: { name: string; message: string }
      ): Promise<Response> => {
        // If authentication is successful.
        if (user) {
          const token = jwt.sign(
            { id: user.id, email: user.email },
            AuthController.config.JWT_SECRET
          );
          return res.status(200).json({
            token: token,
          });
        }
        // Otherwise, send authentication error.
        return res.status(400).json({
          errors: [
            {
              // @ts-ignore
              error: AuthErrors[info.name],
              message: info.message,
            },
          ],
        });
      };
      passport.authenticate('local', { session: false }, afterVerify)(req, res);
    } catch (e) {
      l.error('Login:', e);
      return res.status(500).json({
        errors: [
          {
            error: 'InternalServerError',
            message: 'Internal server error.',
          },
        ],
      });
    }
  }

  public static async logout(req: Request, res: Response): Promise<void> {
    req.logout();
    res.status(200).json();
  }
}
