import { Request, Response, NextFunction } from 'express';
import { validationResult } from 'express-validator';
import { query } from 'express-validator';

export default class WeatherValidator {
  public static readonly weatherValidator = [
    query('id')
      .optional()
      .isString(),
    query(['city', 'country'])
      .optional()
      .isString(),
    query(['lon', 'lat'])
      .optional()
      .isNumeric(),
    query('date')
      .optional()
      .isString(),
  ];

  public static async validate(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void | Response> {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
      return next();
    }
    const extractedErrors: Array<Record<string, string>> = [];
    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }));

    return res.status(400).json({
      errors: extractedErrors,
    });
  }
}
