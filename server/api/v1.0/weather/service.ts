import IORedis from 'ioredis';
import _ from 'lodash';

import { OWMAPI } from '@lib/owm';

import l from '@server/common/logger';
import ServerConfig from '@server/common/config';

import {
  WeatherDocument,
  WeatherModel,
  WeatherModelInterface,
} from '@server/weather';
import ConditionUtils from './utils';

interface OWMService {
  getById(id: string): Promise<WeatherDocument>;

  getByCityName(params: {
    city: string;
    country?: string;
    date: Date;
  }): Promise<WeatherDocument[]>;

  getByCityCoords(params: {
    coords: { lat: string; lon: string };
    date: Date;
  }): Promise<WeatherDocument[]>;
}

export default class WeatherService implements OWMService {
  private readonly api: OWMAPI;
  private readonly cache: IORedis.Redis;
  private readonly config: ServerConfig;
  private readonly model: WeatherModelInterface;

  constructor(
    config: ServerConfig,
    cache: IORedis.Redis,
    model: WeatherModelInterface,
    api: OWMAPI
  ) {
    this.config = config;
    this.cache = cache;
    this.model = model;
    this.api = api;
  }

  /**
   * Get forecast data for a particular location from OWM resource
   * @param params Request query parameters
   */
  public async getForecast(params: object): Promise<WeatherDocument[]> {
    const forecasts: WeatherDocument[] = [];
    try {
      const res: any = await this.api.getForecast(params);
      if (res.cod != 200) {
        l.error('Forecast:', res);
        return;
      }
      for (const entry of res.list) {
        const conditions = await ConditionUtils.getWeatherConditions(
          entry.weather
        );
        const forecast: WeatherDocument = new WeatherModel({
          name: res.city.name,
          country: res.city.country,
          location: {
            type: 'Point',
            coordinates: [entry.coord.lon, entry.coord.lat],
          },
          weather: conditions,
          main: entry.main,
          dt: entry.dt,
          // eslint-disable-next-line @typescript-eslint/camelcase
          dt_txt: entry.dt_txt,
          timezone: res.city.timezone,
        });
        await forecast.save();
        forecasts.push(forecast);
      }
      return Promise.resolve(forecasts);
    } catch (e) {
      l.error('Forecast: ', e);
      return Promise.reject(e);
    }
  }

  /**
   * Get weather conditions for a particular day from OWM resource
   * @param params
   */
  public async getWeather(params: object): Promise<WeatherDocument> {
    try {
      const res: any = await this.api.getWeather(params);
      if (res.cod != 200) {
        l.error('Weather: ', res);
        return;
      }

      const conditions = await ConditionUtils.getWeatherConditions(res.weather);
      const weather: WeatherDocument = new WeatherModel({
        visibility: res.visibility,
        name: res.name,
        country: res.sys.country,
        location: {
          type: 'Point',
          coordinates: [res.coord.lon, res.coord.lat],
        },
        weather: conditions,
        main: res.main,
        dt: res.dt,
        // eslint-disable-next-line @typescript-eslint/camelcase
        dt_txt: res.dt_txt,
        timezone: res.timezone,
      });
      await weather.save();
      return Promise.resolve(weather);
    } catch (e) {
      l.error('Weather:', e);
      return Promise.reject(e);
    }
  }

  /**
   * Get weather condition by a city coordinates
   * @param params database query parameters
   */
  public async getByCityCoords(params: {
    coords: { lat: string; lon: string };
    date: Date;
  }): Promise<WeatherDocument[]> {
    let forecasts: Array<WeatherDocument> = [];
    try {
      forecasts = await this.model
        .find({
          location: {
            $near: {
              $geometry: {
                type: 'Point',
                coordinates: [
                  parseFloat(params.coords.lon),
                  parseFloat(params.coords.lat),
                ],
              },
              $maxDistance: 10,
            },
          },
          dt: {
            $gte: new Date(
              params.date.getFullYear(),
              params.date.getUTCMonth(),
              params.date.getUTCDay()
            ).getTime(),
            $lt: params.date.getTime(),
          },
        })
        .exec();
      if (!_.isEmpty(forecasts)) {
        (async ctx => {
          for (const forecast of forecasts) {
            ctx.cache.set(forecast.id, forecast.toJSON());
          }
        })(this);
      } else {
        this.getWeather(params.coords);
      }
    } catch (e) {
      l.error('Get by city coords: ', e);
    }
    return Promise.resolve(forecasts);
  }

  public async getByCityName(params: {
    city: string;
    country?: string;
    date: Date;
  }): Promise<WeatherDocument[]> {
    let forecasts: Array<WeatherDocument> = [];
    try {
      forecasts = await this.model
        .find({
          name: {
            $regex: new RegExp(params.city, 'i'),
          },
          country: {
            $regex: new RegExp(params.country || '*', 'i'),
          },
          dt: {
            $gte: new Date(
              params.date.getFullYear(),
              params.date.getUTCMonth(),
              params.date.getUTCDay()
            ).getTime(),
            $lt: params.date.getTime(),
          },
        })
        .exec();
      if (!_.isEmpty(forecasts)) {
        (async ctx => {
          for (const forecast of forecasts) {
            ctx.cache.set(forecast.id, forecast.toJSON());
          }
        })(this);
      } else {
        const query = {
          q: `${params.city},${params.country}`,
        };
        this.getWeather(query);
      }
    } catch (e) {
      l.error('Get by city name: ', e);
    }

    return Promise.resolve(forecasts);
  }

  public async getById(id: string): Promise<WeatherDocument> {
    let forecast = JSON.parse(await this.cache.get(id));
    if (!forecast) {
      forecast = await this.model.findById(id);
    }
    return Promise.resolve(forecast);
  }
}
