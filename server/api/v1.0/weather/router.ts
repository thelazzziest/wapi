import { Router } from 'express';
import passport from 'passport';

import WeatherController from './controller';
import WeatherValidator from './validator';

export default class WeatherRouter {
  public readonly router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  private routes() {
    const controller = new WeatherController();
    this.router.get(
      '/today',
      WeatherValidator.weatherValidator,
      WeatherValidator.validate,
      passport.authenticate('jwt', { session: false }),
      controller.weather
    );
    this.router.get(
      '/forecast',
      WeatherValidator.weatherValidator,
      WeatherValidator.validate,
      passport.authenticate('jwt', { session: false }),
      controller.forecast
    );
  }
}
