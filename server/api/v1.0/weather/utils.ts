import { ConditionDocument, ConditionModel } from '@server/weather';

export default class ConditionUtils {
  public static async getWeatherConditions(
    conditionData: Array<{
      id: string;
      main: string;
      description: string;
      icon: string;
    }>
  ): Promise<Array<ConditionDocument>> {
    const conditions: Array<ConditionDocument> = [];
    for (const condition of conditionData) {
      let model = await ConditionModel.findOne({ id: condition.id });
      if (!model) {
        model = new ConditionModel(condition);
        model.save();
      }
      conditions.push(model);
    }
    return conditions;
  }
}
