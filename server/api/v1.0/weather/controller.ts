import { Request, Response } from 'express';
import { Redis } from 'ioredis';

import ServerConfig from '@server/common/config';
import redis from '@server/common/redis';

import {
  WeatherDocument,
  WeatherModel,
  WeatherModelInterface,
} from '@server/weather';

import WeatherService from './service';
import _ from 'lodash';
import { OWMAPI } from '@lib/owm';

export default class WeatherController {
  private static readonly config: ServerConfig = ServerConfig.getInstance();
  private static readonly cache: Redis = redis;
  private static readonly model: WeatherModelInterface = WeatherModel;
  private static readonly api: OWMAPI = new OWMAPI(
    WeatherController.config.OWM_APP_ID,
    { json: true }
  );
  private static readonly service: WeatherService = new WeatherService(
    WeatherController.config,
    WeatherController.cache,
    WeatherController.model,
    WeatherController.api
  );

  public async weather(req: Request, res: Response): Promise<Response> {
    const lonLat: Array<string> = ['lon', 'lat'];
    const loc: Array<string> = ['city', 'country'];
    const now = new Date();

    // Find query parameters
    const id: string = _.pick(req.query, 'id').id;
    const coords: { lat: number; lon: number } | any = _.each(
      _.pick(req.query, lonLat),
      (val: string) => parseFloat(val)
    );
    const locality: { city: string; country: string } | any = _.pick(
      req.query,
      loc
    );

    let weather: Array<WeatherDocument> = [];

    if (id) {
      const entry: WeatherDocument = await WeatherController.service.getById(
        id
      );
      if (entry) {
        weather.push(entry);
      }
    } else if (_.keys(coords).length == 2) {
      weather = await WeatherController.service.getByCityCoords({
        coords: coords,
        date: now,
      });
    } else if (_.keys(locality).length == 2) {
      weather = await WeatherController.service.getByCityName({
        city: locality.city,
        country: locality.country,
        date: now,
      });
    }

    if (_.isEmpty(weather)) {
      return res.status(404);
    } else {
      return res.status(200).json({
        data: weather,
      });
    }
  }

  public async forecast(req: Request, res: Response): Promise<Response> {
    const lonLat: Array<string> = ['lon', 'lat'];
    const loc: Array<string> = ['city', 'country'];
    const now = new Date();
    const forecastDate = new Date();

    // Get query parameters
    const id: string = _.pick(req.query, 'id').id;
    const coords: { lat: number; lon: number } | any = _.each(
      _.pick(req.query, lonLat),
      (val: string) => parseFloat(val)
    );
    const locality: { city: string; country: string } | any = _.pick(
      req.query,
      loc
    );
    const date: string = _.pick(req.query, 'date').date;

    if (!date) {
      forecastDate.setDate(forecastDate.getDate() + 1);
    } else if (Date.parse(date) <= now.getMilliseconds()) {
      return res.status(400).json({
        message: 'You can get weather forecast only for coming days.',
      });
    } else {
      forecastDate.setDate(Date.parse(date));
    }

    let forecast = null;

    if (id) {
      forecast = await WeatherController.service.getById(id);
    } else if (_.keys(coords).length == 2) {
      forecast = await WeatherController.service.getByCityCoords({
        coords: coords,
        date: forecastDate,
      });
    } else if (_.keys(locality).length == 2) {
      forecast = await WeatherController.service.getByCityName({
        city: locality.city,
        country: locality.country,
        date: forecastDate,
      });
    }

    if (_.isEmpty(forecast)) {
      return res.status(404);
    } else {
      return res.status(200).json({
        data: forecast,
      });
    }
  }
}
