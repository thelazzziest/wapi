import { Router } from 'express';

import ExpressServer from '@server/common/server';

import AuthRouter from './auth';
import WeatherRouter from './weather';

export default class V1Router {
  public readonly router: Router;

  constructor(ctx: ExpressServer) {
    this.router = Router();
    this.router.use('/auth', new AuthRouter().router);
    this.router.use('/weather', new WeatherRouter().router);
  }
}
